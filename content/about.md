+++
date = "2017-05-24T00:26:44-05:00"
title = "About"

+++
I am a PhD student in theoretical nuclear physics at Iowa State University. My advisor is [Prof. James P. Vary](www.public.iastate.edu/~jvary). Before that I did my masters in physics from Birla Institute of Technology and Science, India, where I worked on quasinormal modes of black holes for my thesis.

My current research is about the counstruction and application of chiral effective field theory operators for the numerical computation of the so-called nuclear matrix elements for the elusive neutrinoless double beta decay process. This project has exposed me to the highly fascinating fields of high performance computing and deep learning and I am looking forward to opportunities to explore my interests in those domains. 

Outside of my research, I enjoy reading, fiction - mostly, scifi and fantasy - and philosophy, alike. I also frequently rice my Linux box, in search of the yet-to-be found perfect mouseless configuration. And I am a rookie bachata dancer.

The source code for this website is available on [gitlab](https://gitlab.com/e-eight/e-eight.gitlab.io) under a MIT license.

If you wish to contact me, please email me at [soham@iastate.edu](mailto:soham@iastate.edu) or message me on [LinkedIn](https://www.linkedin.com/in/sohampal/).
