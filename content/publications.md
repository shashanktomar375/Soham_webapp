+++
date = "2017-11-29T01:25:01+05:30"
title = "publications"

+++

* S. Pal, K. Rajeev, S. Shankaranarayanan, [*An approach to the quantization of black hole quasi-normal modes*](https://arxiv.org/abs/1312.5689), Int. J. Mod. Phys. D, 24, 11 (2015)
