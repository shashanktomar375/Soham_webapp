+++
date = "2017-11-29T18:13:46+05:30"
title = "resume"

+++

[My resume](https://gitlab.com/e-eight/resume/blob/master/soham-resume.pdf) is hosted on gitlab, which renders the pdf for you 
(usually works best with a [PDF.js](https://mozilla.github.io/pdf.js/) enabled browser). If you cannot see the pdf or want to download it, then please click the download button.

The [LaTeX source code](https://gitlab.com/e-eight/resume/blob/master/soham-resume.tex) of my resume is also available. Feel free to fork and use the layout for your own resume.
